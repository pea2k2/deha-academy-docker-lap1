<?php
$servername = "mysql-db"; // or the service name defined in docker-compose.yml
$username = "db_user";
$password = "password";
$database = "test_database";

$conn = mysqli_connect($servername, $username, $password, $database);

// Check if the connection was successful
if ($conn->connect_error) {
    // Display an error message and terminate the script if the connection fails
    die("Connection failed: " . $conn->connect_error);
}

// If the connection is successful, print a success message
echo "PHP Connected to MySQL successfully";

// Close the database connection
$conn->close();
?>